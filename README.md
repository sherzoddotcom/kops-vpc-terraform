# Terraform configuration for kOps tutorial VPC

## Prerequisites

`terraform ~> 1.1`

## Configuration

Adjust values in `providers.tf.sample` and `backend.tf.sample` (optional), and rename them without `.sample`. If this Terraform configuration is used in non-prod, non-work environment, configuration backend is optional but highly recommended as it will require little to no S3 resources (and costs, as they will fit into free-tier).
