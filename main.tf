module "kops-vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.11.5"

  name = "kops-vpc"
  cidr = "10.2.0.0/22"

  azs             = ["us-east-1a", "us-east-1b",]
  public_subnets  = ["10.2.0.0/26", "10.2.0.64/26"]
  private_subnets = ["10.2.2.0/25", "10.2.2.128/25",]

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true
}

output "vpc-cidr" {
  value = module.kops-vpc.vpc_cidr_block
}

output "vpc-id" {
  value = module.kops-vpc.vpc_id
}

output "private_subnet_ids" {
  value = module.kops-vpc.private_subnets
}

output "public_subnet_ids" {
  value = module.kops-vpc.public_subnets
}

output "private_subnet_cidr_block" {
  value = module.kops-vpc.private_subnets_cidr_blocks
}

output "public_subnet_cidr_block" {
  value = module.kops-vpc.public_subnets_cidr_blocks
}

output "nat_gateways" {
  value = module.kops-vpc.natgw_ids
}
